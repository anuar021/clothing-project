import React from "react";
import "./user-dropdown.styles.scss";
import {getCategoriesAndDocuments, getUsers, signOutUser} from "../../utils/firebase/firebase.utils";
import {useSelector} from "react-redux";
import {selectCurrentUser} from "../../store/user/user.selector";

const UserDropdown = () => {
    const currentUser = useSelector(selectCurrentUser);

    const redirectToAdminPanel = () => {
        // window.location.href = `http://localhost:3001/dashboard/app?username=${encodeURIComponent(currentUser.displayName)}`
        console.log(currentUser)
    };

    const getCategoriesMap = async () => {
        console.log(currentUser)

        const categoriesArray = await getUsers();
        console.log(categoriesArray)
    };

    return (
        <div className="user-dropdown-container">
            <div className="user-items">
                <div className="item">Profile</div>
                <div className="item" onClick={getCategoriesMap}>Admin panel</div>
                <div className="item" onClick={signOutUser}>
                    Logout
                </div>
            </div>
        </div>
    );
};

export default UserDropdown;
